import discord # Imports the discord.py library
from discord.ext.commands import Bot # Imports the discordpy commands lib
import asyncio # Imports asyncio
import logging # Imports the logger
import config # Imports config.py

print('===========')
print('Connecting To Discord...')
print('===========')

logging.basicConfig(level=logging.INFO) # Configurates the logger
logger = logging.getLogger('discord')

client = Bot(command_prefix=config.PREFIX) # Sets the client and sets the prefix to what is defined in "config.py"

client.remove_command("help") # Removed the default help command

@client.event
async def on_ready(): # On the ready event
    await client.change_presence(game=discord.Game(name='u!help')) # Sets the current "playing" status
    print('') # Prints the Bot's username and ID on login

@client.command(pass_context=True)
# Sends some info about the Bot in a Discord RichEmbed
async def info(ctx): # ctx = context
    embed = discord.Embed()
    embed.title = "Info"
    embed.description = "The Info and Stats of United YouTubers Bot"
    embed.add_field(name="Status", value="Online :thumbsup: ", inline=True)
    embed.add_field(name="Version", value=config.VERSION, inline=True)
    embed.add_field(name="Library", value="discord.py", inline=True)
    embed.add_field(name="Website", value="http://bot.unitedyoutubers.tk", inline=True)

    return await client.send_message(destination=ctx.message.channel, embed=embed)

@client.command()
# Sends the user their earnings for the Month
async def earnings():
    # Insert code to link with earnings database when implemented
    await client.say("This service is not yet implemented. For now, have a picture of a Lama!")
    return await client.say("https://www.mememaker.net/static/images/memes/4586780.jpg")

@client.command()
# Sends the user a list of commands they can use
async def help():
    return await client.say("To get a list of commands please visit http://bot.unitedyoutubers.tk")

client.run(config.TOKEN)